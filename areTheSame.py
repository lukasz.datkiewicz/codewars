def comp(array1, array2):
    if array1 == [] or array2 == []:
        return False
    else:
        array1 = list(map(lambda x: x**2,array1))
        return True if array1.sort() == array2.sort() else False

#def comp(array1, array2):
#    try:
#        return sorted([i**2 for i in array1]) == sorted (array2)
#    except:
#        return False

a = [121, 144, 19, 161, 19, 144, 19, 11]  
b = [11*11, 121*121, 144*144, 19*19, 161*161, 19*19, 144*144, 19*19]


print(comp(a,b))