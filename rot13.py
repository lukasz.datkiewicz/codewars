def rot13(message):
    m = []
    for i in message:
        if 64 < ord(i) < 78 or 96 < ord(i) < 110:
            m.append(chr(ord(i)+13))
        elif 77 < ord(i) < 91 or 109 < ord(i) < 123:
            m.append(chr(ord(i)-13))
        else:
            m.append(i)
    return ''.join(m)

