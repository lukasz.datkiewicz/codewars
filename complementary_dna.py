dna = "ATTGC"

def DNA_strand(dna):
    return dna.translate(dna.maketrans("ATCG", "TAGC"))


def DNA_strand2(dna):
    pairs = {'A':'T','T':'A','C':'G','G':'C'}
    return ''.join([pairs[x] for i in dna])

print(DNA_strand(dna)) 