def decrypt(encrypted_text, n):
    if encrypted_text == '' or n <= 0:
        return encrypted_text

    ndx = len(encrypted_text) // 2

    a = encrypted_text[:ndx]
    b = encrypted_text[ndx:]
    
    encrypted_text = ''.join(b[i:i+1] + a[i:i+1] for i in range (ndx +1))
    return decrypt(encrypted_text,n-1)
    


def encrypt(text, n):
    if text == '' or n <= 0:
        return text
    return encrypt(text[1::2] + text[::2], n-1)