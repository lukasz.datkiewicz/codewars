def bouncingBall(h, bounce, window):
    if ((h < 0) or not (0 < bounce < 1) or (window > h) ) :
        return -1
    counter = 0
    while (h > window):
        h = h * bounce
        counter += 2
    return counter -1