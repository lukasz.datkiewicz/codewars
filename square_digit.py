num = 9119

def square_digits(num):
    num = [str(int(d)**2) for d in str(num)]
    return int(''.join(num))


print(square_digits(num))