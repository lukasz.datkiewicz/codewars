import string

test = ['The quick, brown fox jumps over the lazy dog!']


def is_pangram(s):
    return set(s.lower()) >= set(string.ascii_lowercase)

print(is_pangram(test[0]))